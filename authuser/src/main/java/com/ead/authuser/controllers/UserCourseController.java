package com.ead.authuser.controllers;

import com.ead.authuser.clients.CourseClient;
import com.ead.authuser.dtos.CourseDto;
import com.ead.authuser.dtos.UserCourseDto;
import com.ead.authuser.models.UserCourseModel;
import com.ead.authuser.models.UserModel;
import com.ead.authuser.service.UserCourseService;
import com.ead.authuser.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;
import java.util.UUID;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserCourseController {

    @Autowired
    private UserService userService;
    @Autowired
    private CourseClient courseClient;

    @Autowired
    private UserCourseService userCourseService;

    @GetMapping("/users/{userId}/courses")
    public ResponseEntity<Object> getAllCoursersByUser(@PageableDefault(page = 0, size = 10, sort = "courseId", direction = Sort.Direction.ASC) Pageable pageable,
                                                                @PathVariable(value = "userId") UUID userId) {
        Optional<UserModel> userModelOptional = userService.findById(userId);
        if(!userModelOptional.isPresent()){
            return  ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found.");
        }
        var result = this.courseClient.getAllCoursesByUsers(userId, pageable);

        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @PostMapping("/users/{userId}/courses/subscription")
    public ResponseEntity<Object> saveSubscriptionInCourse(@PathVariable(value = "userId") UUID userId,
                                                           @RequestBody @Valid UserCourseDto userCourseDto){


        Optional<UserModel> userModelOptional = userService.findById(userId);

        if (!userModelOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found.");
        }

        var test =this.userCourseService.existsByUserAndCourseId(userModelOptional.get(), userCourseDto.getCourseId());

        if(this.userCourseService.existsByUserAndCourseId(userModelOptional.get(), userCourseDto.getCourseId())){
            return ResponseEntity.status(HttpStatus.CONFLICT).body("Subscription already exists.");
        }

        UserCourseModel userCourseModel = this.userCourseService.save(userModelOptional.get().convertToUserCourseModel(userCourseDto.getCourseId()));


        return ResponseEntity.status(HttpStatus.CREATED).body(userCourseModel);
    }


    @DeleteMapping("/users/courses/{courseId}")
    public  ResponseEntity<Object> deleteUserCourseByCourse(@PathVariable(value = "courseId") UUID courseId){

        if(this.userCourseService.existsByCourseId(courseId)){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("UserCourse not found");
        }

        this.userCourseService.deleteUserCourseByCourse(courseId);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("UserCourse deleted successfuly");
    }
}
