package com.ead.authuser.service.impl;

import com.ead.authuser.models.UserCourseModel;
import com.ead.authuser.models.UserModel;
import com.ead.authuser.repository.UserCourseRepository;
import com.ead.authuser.service.UserCourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class UserCourseServiceImpl implements UserCourseService {

    @Autowired
    private UserCourseRepository userCourseReposity;


    @Override
    public boolean existsByUserAndCourseId(UserModel userModel, UUID courseId) {
        return userCourseReposity.existsByUserAndCourseId(  userModel,   courseId);
    }

    @Override
    public UserCourseModel save(UserCourseModel userCourseModel) {
        return this.userCourseReposity.save(userCourseModel);
    }

    @Override
    public boolean existsByCourseId(UUID courseId) {
        return this.userCourseReposity.existsByCourseId(courseId);
    }

    @Transactional
    @Override
    public void deleteUserCourseByCourse(UUID courseId) {
        this.userCourseReposity.deleteUserCourseByCourseId(courseId);
    }
}
