package com.ead.authuser.clients;

import com.ead.authuser.dtos.CourseDto;
import com.ead.authuser.dtos.ResponsePageDto;
import com.ead.authuser.service.UtilsService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@Log4j2
@Component
public class CourseClient {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UtilsService utilsService;

    @Value("${ead.api.url.course}")
    String RESQUEST_URL_COURSE;

    public Page<CourseDto> getAllCoursesByUsers(UUID userId, Pageable pageable) {
        List<CourseDto> searchResult = null;

        String url = RESQUEST_URL_COURSE + this.utilsService.createUrl(userId, pageable);

        log.debug("Request URL: {}", url);
        log.info("Request URL: {}", url);

        try {

            ParameterizedTypeReference<ResponsePageDto<CourseDto>> responseType = new ParameterizedTypeReference<ResponsePageDto<CourseDto>>() {
            };

            ResponseEntity<ResponsePageDto<CourseDto>> result = this.restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            searchResult = result.getBody().getContent();

        } catch (HttpStatusCodeException e) {
            log.error("Error request /courses {}", e);
        }

        log.info("End request /courses userId {}", userId);

        assert searchResult != null;
        return new PageImpl<>(searchResult);
    }


    public void deleteUserInCourse(UUID userId) {

        String url = RESQUEST_URL_COURSE + "/course/users/" + userId;

        this.restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
    }
}
