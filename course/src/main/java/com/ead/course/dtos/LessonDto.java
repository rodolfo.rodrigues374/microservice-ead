package com.ead.course.dtos;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LessonDto {

    @NotBlank
    private String Title;

    private String Description;
    @NotBlank
    private String videoUrl;
}
