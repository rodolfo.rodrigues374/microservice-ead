package com.ead.course.client;

import com.ead.course.dtos.ResponsePageDto;
import com.ead.course.dtos.UserCourseDto;
import com.ead.course.dtos.UserDto;
import com.ead.course.service.UtilsService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@Log4j2
@Component
public class AuthUserClient {


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UtilsService utilsService;

    @Value("${ead.api.url.auth-user}")
    String RESQUEST_URL_AUTHUSER;

    public Page<UserDto> getAllUsersByCourses(UUID courseId, Pageable pageable) {
        List<UserDto> searchResult = null;

        String url = RESQUEST_URL_AUTHUSER + this.utilsService.createUrlGetAllUsersByCourse(courseId, pageable);

        log.debug("Request URL: {}", url);
        log.info("Request URL: {}", url);

        try {

            ParameterizedTypeReference<ResponsePageDto<UserDto>> responseType = new ParameterizedTypeReference<ResponsePageDto<UserDto>>() {
            };

            ResponseEntity<ResponsePageDto<UserDto>> result = this.restTemplate.exchange(url, HttpMethod.GET, null, responseType);

            searchResult = result.getBody().getContent();

        } catch (HttpStatusCodeException e) {
            log.error("Error request /courses {}", e);
        }

        log.info("End request /courses userId {}", courseId);

        assert searchResult != null;
        return new PageImpl<>(searchResult);
    }

    public ResponseEntity<UserDto> getOneUserById(UUID userId) {
        String url = RESQUEST_URL_AUTHUSER + "/users/" + userId;

        return restTemplate.exchange(url, HttpMethod.GET, null, UserDto.class);
    }

    public void postSubScriptionUserInCourse(UUID courseId, UUID userId) {

        String url = RESQUEST_URL_AUTHUSER + "/users/" + userId + "/courses/subscription";

        var courseUserDto = new UserCourseDto();
        courseUserDto.setCourseId(courseId);
        courseUserDto.setUserId(userId);

        restTemplate.postForObject(url, courseUserDto, String.class);


    }

    public void deleteCourseUserInAuthtUser(UUID courseId) {

        String url = RESQUEST_URL_AUTHUSER + "/users/courses/" + courseId;
        restTemplate.exchange(url,HttpMethod.DELETE, null,String.class);
    }
}
